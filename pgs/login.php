<?php
if (isset($_SESSION['id'])) {
    header('Location: /');
}
if (!empty($_POST['name']) && !empty($_POST['password'])) {
    $filters = [
        "name" => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        "password" => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
    ];
    $data = filter_var_array($_POST, $filters);

    $stmt = $dbc->prepare("SELECT id,password FROM users WHERE name = ?");
    $stmt->bind_param('s', $data['name']);
    $stmt->execute();
    $stmt->bind_result($id, $password);
    $stmt->store_result();
    if ($stmt->num_rows < 1) {
        echo "<p class='failure-alert'>No user found.</p>";
    }
    while ($stmt->fetch()) {
        if (password_verify($data['password'], $password)) {
            $_SESSION['id'] = $id;
            if (isset($_POST['remember-me'])) {
                setcookie("id", $id, time() + 60 * 60 * 24 * 30, "/", "handpage.tk", true, true);
            }
            header("Location: /");
        } else {
            echo "<p class='failure-alert'>Invalid password.</p>";
        }
    }
}
?>
<form action="" method="post">
    <label for="name">Name:</label>
    <input id="name" type="text" name="name">
    <label for="password">Password:</label>
    <input id="password" type="password" name="password">
    <label for="remember-me">Remember Me:</label>
    <input id="remeber-me" name="remember-me" type="checkbox">
    <input type="submit" name="submit" value="Login">
</form>
