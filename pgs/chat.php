<?php
if (!isset($_SESSION['id'])) {
    header("Location: /login");
}

if (isset($_GET['chat_id_to_clear']) && !empty($_GET['chat_id_to_clear']) && isset($_GET['clear'])) {
    if (isset($_POST['password']) && !empty($_POST['password'])) {

        $filters = [
            "password" => FILTER_SANITIZE_SPECIAL_CHARS,
        ];

        $data = filter_var_array($_POST, $filters);

        // Get password and verify
        $stmt = $dbc->prepare("SELECT user_id,password FROM chats WHERE id = ?");
        $stmt->bind_param('i', $_GET['chat_id_to_clear']);
        $stmt->execute();
        $stmt->bind_result($user_id, $password);
        while ($stmt->fetch()) {
            if ($user_id == $_SESSION['id']) {
                if (password_verify($data['password'], $password)) {
                    // Clear Chat
                    $stmt->prepare("DELETE FROM chat WHERE chat_id = ?");
                    $stmt->bind_param('i', $_GET['chat_id_to_clear']);
                    if ($stmt->execute()) {
                        echo "<p class='success-alert'>Chat room cleared succesfully. <a href='/chat'>Back</a></p>";
                    } else {
                        echo "<p class='failure-alert'>Chat room clearing failed!  <a href='/chat'>Back</a></p>";
                    }

                } else {
                    echo "<p class='failure-alert'>Password incorrect <a href='/chat'>Back</a></p>";
                }
            } else {
                echo "<p class='failure-alert'>You are not the owner of this chat room! <a href='/chat'>Back</a></p>";
            }
        }
    } else {
        $stmt = $dbc->prepare("SELECT user_id,password,name FROM chats WHERE id = ?");
        $stmt->bind_param('i', $_GET['chat_id_to_clear']);
        $stmt->execute();
        $stmt->bind_result($user_id, $password, $name);
        $stmt->fetch();
        if ($password == "") {
            if ($user_id == $_SESSION['id']) {
                // Delete Chat
                $stmt->prepare("DELETE FROM chat WHERE chat_id = ?");
                $stmt->bind_param('i', $_GET['chat_id_to_clear']);
                if ($stmt->execute()) {
                    echo "<p class='success-alert'>Chat room cleared succesfully. <a href='/chat'>Back</a></p>";
                } else {
                    echo "<p class='failure-alert'>Chat room clearing failed! <a href='/chat'>Back</a></p>";
                }
            } else {
                echo "<p class='failure-alert'>You are not the owner of this chat room! <a href='/chat'>Back</a></p>";
            }
        } else {
            ?>
            <h2>Enter password to confirm clear of: <?php echo $name ?></h2>
            <form method="post">
                <label for="password">Password:</label>
                <input type="password" name="password">
                <input type="submit" name="submit" value="Confirm">
            </form>
            <?php
}
    }
    exit;
}

if (isset($_GET['chat_id_to_delete']) && !empty($_GET['chat_id_to_delete']) && isset($_GET['delete'])) {
    if (isset($_POST['password']) && !empty($_POST['password'])) {

        $filters = [
            "password" => FILTER_SANITIZE_SPECIAL_CHARS,
        ];

        $data = filter_var_array($_POST, $filters);

        // Get password and verify
        $stmt = $dbc->prepare("SELECT user_id,password FROM chats WHERE id = ?");
        $stmt->bind_param('i', $_GET['chat_id_to_delete']);
        $stmt->execute();
        $stmt->bind_result($user_id, $password);
        while ($stmt->fetch()) {
            if ($user_id == $_SESSION['id']) {
                if (password_verify($data['password'], $password)) {
                    // Delete Chat
                    $stmt->prepare("DELETE FROM chats WHERE id = ?");
                    $stmt->bind_param('i', $_GET['chat_id_to_delete']);
                    if ($stmt->execute()) {
                        echo "<p class='success-alert'>Chat room deleted succesfully. <a href='/chat'>Back</a></p>";
                    } else {
                        echo "<p class='failure-alert'>Chat room deletion failed! <a href='/chat'>Back</a></p>";
                    }

                } else {
                    echo "<p class='failure-alert'>Password incorrect <a href='/chat'>Back</a></p>";
                }
            } else {
                echo "<p class='failure-alert'>You are not the owner of this chat room! <a href='/chat'>Back</a></p>";
            }
        }
    } else {
        $stmt = $dbc->prepare("SELECT user_id,password,name FROM chats WHERE id = ?");
        $stmt->bind_param('i', $_GET['chat_id_to_delete']);
        $stmt->execute();
        $stmt->bind_result($user_id, $password, $name);
        $stmt->fetch();
        if ($password == "") {
            if ($user_id == $_SESSION['id']) {
                // Delete Chat
                $stmt->prepare("DELETE FROM chats WHERE id = ?");
                $stmt->bind_param('i', $_GET['chat_id_to_delete']);
                if ($stmt->execute()) {
                    echo "<p class='success-alert'>Chat room deleted succesfully. <a href='/chat'>Back</a></p>";
                } else {
                    echo "<p class='failure-alert'>Chat room deletion failed! <a href='/chat'>Back</a></p>";
                }
            } else {
                echo "<p class='failure-alert'>You are not the owner of this chat room! <a href='/chat'>Back</a></p>";
            }
        } else {
            ?>
            <h2>Enter password to confirm delete of: <?php echo $name ?></h2>
            <form method="post">
                <label for="password">Password:</label>
                <input type="password" name="password">
                <input type="submit" name="submit" value="Confirm">
            </form>
            <?php
}
    }
    exit;
}

if (!isset($_SESSION['chat_id']) && isset($_GET['chat_id']) && !empty($_GET['chat_id'])) {
    // They haven't logged in

    // Get chat name and check if it has a password
    $stmt = $dbc->prepare("SELECT name,password FROM chats WHERE id = ?");
    $stmt->bind_param('i', $_GET['chat_id']);
    $stmt->execute();
    $stmt->bind_result($name, $password);
    $chat_name = "";
    while ($stmt->fetch()) {
        if (empty($password)) {
            $_SESSION['chat_id'] = $_GET['chat_id'];
            header("Location: /chat?chat_id=" . $_SESSION['chat_id']);
        }
        $chat_name = $name;
    }

    ?>
            <h2>Login to <?php echo $chat_name ?></h2>
            <form method="post">
                <label for="password">Password:</label>
                <input type="password" name="password">
                <input type="submit" name="submit" value="Login">
            </form>

        <?php
}

if (isset($_SESSION['chat_id'])) {

    if (isset($_GET['leave'])) {
        unset($_SESSION['chat_id']);
        header("Location: /chat");
    } else {

        if (isset($_POST['message']) && !empty($_POST['message'])) {
            $filters = [
                "chat_id" => ["filter" => FILTER_VALIDATE_INT, "options" => ["min_range" => 1]],
                "message" => FILTER_SANITIZE_SPECIAL_CHARS,
            ];
            $input = ["chat_id" => $_SESSION['chat_id'], "message" => $_POST['message']];
            $data = filter_var_array($input, $filters);

            // Get messages for this chat
            $stmt = $dbc->prepare("INSERT INTO chat (user_id,chat_id,message) VALUES (?,?,?)");
            $stmt->bind_param('iis', $_SESSION['id'], $_SESSION['chat_id'], $_POST['message']);
            $stmt->execute();
            $stmt->store_result();
            if ($stmt->affected_rows < 1) {
                echo "<p class='failure-alert'>Failed to send message. <a href='/chat'>Back</a></p>";
            }
        }

        // Get chat name
        $result = $dbc->query("SELECT name FROM chats WHERE id =" . $_SESSION['chat_id']);
        $chat_name = $result->fetch_all(MYSQLI_ASSOC)[0]['name'];

        echo "<h2>Logged in to chat: " . $chat_name . "</h2>";
        // Get messages for this chat
        $stmt = $dbc->prepare("SELECT * FROM (SELECT chat.id as id,chat.message,chat.created_at,users.name FROM chat JOIN users ON chat.user_id = users.id WHERE chat_id = ? ORDER BY chat.id DESC LIMIT 10) As s ORDER BY id");
        $stmt->bind_param('i', $_SESSION['chat_id']);
        $stmt->execute();
        $stmt->bind_result($id, $message, $created_at, $name);
        echo "<ul id='chat-messages'>";
        while ($stmt->fetch()) {
            echo "<li>[" . $created_at . "] <strong>" . $name . "</strong> " . $message . "</li>";
        }
        echo "</ul>";
        ?>
            <a id='leave-link' href="<?php echo '/chat?chat_id=' . $_SESSION['chat_id']; ?>">Refresh Chat</a>
<br>
                <form method="post">
                    <label for="message">Message:</label>
                    <textarea name="message" rows="10" cols="50" resize="none"></textarea>
                    <input type="submit" name="submit" value="Send">
                </form>
                <a id='leave-link' href="?leave">Leave chat</a>
            <?php
}
    exit;
}

if (isset($_GET['chat_id']) && !empty($_GET['chat_id'])) {
    $filters = [
        "chat_id" => ["filter" => FILTER_VALIDATE_INT, "options" => ["min_range" => 1]],
    ];

    $data = filter_var_array($_GET, $filters);

    if (isset($_POST['password']) && !empty($_POST['password'])) {
        $filters = [
            "password" => FILTER_SANITIZE_SPECIAL_CHARS,
        ];

        $data2 = filter_var_array($_POST, $filters);

        // Get password and verify
        $stmt = $dbc->prepare("SELECT password FROM chats WHERE id = ?");
        $stmt->bind_param('i', $data['chat_id']);
        $stmt->execute();
        $stmt->bind_result($password);
        while ($stmt->fetch()) {
            if (password_verify($data2['password'], $password)) {
                $_SESSION['chat_id'] = $data['chat_id'];
                header("Location: /chat?chat_id=" . $_SESSION['chat_id']);
            } else {
                echo "<p class='failure-alert'>Password incorrect</p>";
            }
        }
    }

} else {

    if (!empty($_POST['name'])) {
        $filters = [
            "name" => FILTER_SANITIZE_SPECIAL_CHARS,
            "password" => FILTER_SANITIZE_SPECIAL_CHARS,
        ];

        $data = filter_var_array($_POST, $filters);

        // Check there is not already a chat room with provided name
        $stmt = $dbc->prepare("SELECT id FROM chats WHERE name = ?");
        $stmt->bind_param('s', $data['name']);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            echo "<p class='failure-alert'>Chat room name taken.</p>";
        } else {
            if (!empty($data['password'])) {
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            }

            $stmt = $dbc->prepare("INSERT INTO chats (name,user_id,password) VALUES (?,?,?)");
            $stmt->bind_param('sis', $data['name'], $_SESSION['id'], $data['password']);
            $stmt->execute();
            $stmt->store_result();
            if ($stmt->affected_rows > 0) {
                echo "<p class='success-alert'>Chat room created succesfully.</p>";
            } else {
                echo "<p class='failure-alert'>Failed to create chat room.</p>";
            }
        }
    }
    echo "<h2>Available Chat Rooms</h2>";

    $stmt = $dbc->prepare("SELECT id,user_id,name,password FROM chats");
    $stmt->execute();
    $stmt->bind_result($id, $user_id, $name, $password);
    echo "<ul>";
    while ($stmt->fetch()) {
        if (empty($password)) {
            echo "<li><a href='?chat_id=$id'>$name - Public</a>";
        } else {
            echo "<li><a href='?chat_id=$id'>$name - Private</a>";
        }
        if ($user_id == $_SESSION['id']) {
            echo " - <a href='?chat_id_to_clear=$id&clear'>Clear</a>";
            echo " - <a href='?chat_id_to_delete=$id&delete'>Delete</a>";
        }
        echo "</li>";
    }
    echo "</ul>";
    ?>
<h2>Create Chat Room</h2>
<form method="post">
    <label for="name">Chat Name</label>
    <input id="name" type="text" name="name">
    <label for="password">Password (leave blank for public)</label>
    <input id="password" type="password" name="password">
    <input type="submit" type="submit" value="Create Room">
</form>
<?php
}
?>