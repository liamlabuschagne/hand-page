<?php
if (!isset($_SESSION['id'])) {
    header("Location: /login");
}
if (isset($_GET['delete_account'])) {
    if (isset($_POST['password']) && !empty($_POST['password'])) {
        $filters = [
            "password" => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        ];
        $data = filter_var_array($_POST, $filters);

        $stmt = $dbc->prepare("SELECT password FROM users WHERE id = ?");
        $stmt->bind_param('i', $_SESSION['id']);
        $stmt->execute();
        $stmt->bind_result($password);
        $stmt->store_result();

        while ($stmt->fetch()) {
            if (password_verify($data['password'], $password)) {
                $stmt->prepare("DELETE FROM users WHERE id = ?");
                $stmt->bind_param('i', $_SESSION['id']);
                if ($stmt->execute()) {
                    echo "<p class='success-alert'>Account deleted, sorry to see you go. <a href='/logout'>Logout</a></p>";
                } else {
                    echo "<p class='failure-alert'>Failed to delete account. <a href='/profile'>Back</a></p>";
                }
            } else {
                echo "<p class='failure-alert'>Invalid password. <a href='/profile'>Back</a></p>";
            }
        }
    } else {
        ?>
            <h2>Enter password to confirm account deletion.</h2>
            <form action="" method="post">
                <label for="password">Password:</label>
                <input id="password" type="password" name="password">
                <input type="submit" name="submit" value="Login">
            </form>

        <?php
}
    exit;
}

if (isset($_GET['change_username'])) {
    if (isset($_POST['username']) && !empty($_POST['username'])) {
        if (preg_match('/\s/', $_POST['username'])) {
            echo "<p class='failure-alert'>No spaces are allowed in your user name.</p>";
            exit;
        }
        $filters = [
            "username" => FILTER_SANITIZE_SPECIAL_CHARS,
        ];

        $data = filter_var_array($_POST, $filters);

        $stmt = $dbc->prepare("SELECT id FROM users WHERE name = ?");
        $stmt->bind_param('s', $data['username']);
        $stmt->execute();
        $stmt->store_result();
        if (!$stmt->num_rows < 1) {
            echo "<p class='failure-alert'>Username taken. <a href='/profile'>Back</a></a>";
            exit;
        }
        $stmt = $dbc->prepare("UPDATE users SET name = ? WHERE id = ?");
        $stmt->bind_param('si', $data['username'], $_SESSION['id']);
        if ($stmt->execute()) {
            echo "<p class='success-alert'>Changed Successfully</p>";
        } else {
            echo "<p class='failure-alert'>Change Failed! <a href='/profile'>Back</a></p>";
        }
    } else {
        ?>
            <h2>Change Username</h2>
            <form method="post">
                <label for="username">Username:</label>
                <input type="username" name="username">
                <input type="submit" name="submit" value="Change">
            </form>
        <?php
}
    exit;
}

if (isset($_FILES['image']['tmp_name']) && !empty($_FILES['image']['tmp_name']) && isset($_POST['submit'])) {
    $target_file = "img/profiles/" . $_SESSION['id'];
    // Check if image file is a actual image or fake image
    if (!getimagesize($_FILES["image"]["tmp_name"])) {
        echo "<p class='failure-alert'>File is not an image.</p>";
    } else {
        // Check file size
        if ($_FILES["image"]["size"] > 5000000) {
            echo "<p class='failure-alert'>Sorry, your file is too large. (5mb Max)</p>";
        } else {
            // Delete previous file
            if (file_exists('/img/profiles/' . $_SESSION['id'])) {
                unlink($target_file);
            }

            // Do the upload
            if (!move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                echo "<p class='failure-alert'>Could not upload image.</p>";
                exit;
            }
            echo "<p class='success-alert'>Uploaded Successfully</p>";
        }

    }
}
$stmt = $dbc->prepare("SELECT name,created_at FROM users WHERE id = ?");
$stmt->bind_param('i', $_SESSION['id']);
$stmt->execute();
$stmt->bind_result($name, $created_at);
while ($stmt->fetch()) {
    echo "<ul id='profile'><li>";
    if (file_exists('img/profiles/' . $_SESSION['id'])) {
        echo "<img class='profile-img' src='/img/profiles/" . $_SESSION['id'] . "' alt='Profile Image'>";
    } else {
        echo "<img class='profile-img' src='/img/profiles/default' alt='Default Profile Image'>";

    }
    echo "</li><li><p>Username: " . $name . " <a href='?change_username'>Change</a><br>";
    echo "Creation Date: " . $created_at . "</p></li>";
    echo "</ul>";
}
?>
<h2>Upload Profile Image</h2>
<form method="post" enctype="multipart/form-data">
    <p>Choose File:</p>
    <input type="file" name="image" id="image">
    <input type="submit" value="Upload Image" name="submit">
</form>

<a id="delete" href="?delete_account">Delete Account</a>