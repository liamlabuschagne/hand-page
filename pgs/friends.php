<?php
if (!isset($_SESSION['id'])) {
    header("Location: /login");
}

if (isset($_GET['delete']) && !empty($_GET['delete'])) {
    $filters = [
        "delete" => ["filter" => FILTER_VALIDATE_INT, "options" => ["min_range" => 1]],
    ];

    $data = filter_var_array($_GET, $filters);

    $stmt = $dbc->prepare("DELETE FROM friends WHERE (user_id_1 = ? AND user_id_2 = ?) OR (user_id_1 = ? AND user_id_2 = ?)");
    $stmt->bind_param('iiii', $data['delete'], $_SESSION['id'], $_SESSION['id'], $data['delete']);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->affected_rows < 1) {
        echo "<p class='failure-alert'>Could not remove friend. <a href='/friends'>Back</a></p>";
    } else {
        echo "<p class='success-alert'>Friend Removed. <a href='/friends'>Back</a></p>";
    }
    exit;
}

if (isset($_GET['accept'])) {
    $filters = [
        "accept" => ["filter" => FILTER_VALIDATE_INT, "options" => ["min_range" => 1]],
    ];

    $data = filter_var_array($_GET, $filters);

    $stmt = $dbc->prepare("UPDATE friends SET accepted = 1 WHERE id = ?");
    $stmt->bind_param('i', $data['accept']);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->affected_rows < 1) {
        echo "<p class='failure-alert'>Could not accept request. <a href='/friends'>Back</a></p>";
    } else {
        echo "<p class='success-alert'>Friend Added! <a href='/friends'>Back</a></p>";
    }
    exit;
}

if (!empty($_POST['name'])) {
    $filters = [
        "name" => FILTER_SANITIZE_SPECIAL_CHARS,
    ];

    $data = filter_var_array($_POST, $filters);

    // Get friends id
    $stmt = $dbc->prepare("SELECT id FROM users WHERE name = ?");
    $stmt->bind_param('s', $data['name']);
    $stmt->execute();
    $stmt->bind_result($id);
    $stmt->store_result();
    if ($stmt->num_rows > 0) {
        $friendId = 0;

        while ($stmt->fetch()) {
            $friendId = $id;
        }

        // Check that this friendship doesn't already exist
        $stmt->prepare("SELECT id FROM friends WHERE (user_id_1 = ? AND user_id_2 = ?) OR (user_id_1 = ? AND user_id_2 = ?)");
        $stmt->bind_param('iiii', $_SESSION['id'], $friendId, $friendId, $_SESSION['id']);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->affected_rows > 0) {
            echo "<p class='failure-alert'>You are already friends!</p>";
        } else {
            $stmt = $dbc->prepare("INSERT INTO friends (user_id_1,user_id_2) VALUES (?,?)");
            $stmt->bind_param('ii', $_SESSION['id'], $friendId);
            $stmt->execute();
            $stmt->store_result();
            if ($stmt->affected_rows > 0) {
                echo "<p class='success-alert'>Friend Request Sent!</p>";
            } else {
                echo "<p class='failure-alert'>Failed to send request</p>";
            }
        }
    } else {
        echo "<p class='failure-alert'>User Not Found</p>";
    }

}

// Print out friend requests
$stmt = $dbc->prepare("SELECT id,user_id_1 FROM friends WHERE user_id_2 = ? AND accepted = 0");
$stmt->bind_param('s', $_SESSION['id']);
$stmt->execute();
$stmt->bind_result($id, $friendID);

$data = [];

while ($stmt->fetch()) {
    $data = [$id, $friendID];
}

$stmt->close();

// Get user name for request
$stmt = $dbc->prepare("SELECT name FROM users WHERE id = ?");
$stmt->bind_param('i', $data[1]);
$stmt->execute();
$stmt->bind_result($friendName);
$stmt->store_result();
if ($stmt->num_rows > 0) {
    echo "<h2>Friend Requests</h2>";
    echo "<ul>";

    while ($stmt->fetch()) {
        echo "<li><a href='/friends?accept=" . $id . "'>Accept: " . $friendName . "</a></li>";
    }

    echo "</ul>";
}

// Friend list
$stmt = $dbc->prepare("SELECT users.id,users.name FROM users JOIN friends ON ((friends.user_id_1 = users.id AND friends.user_id_2 = ?) OR (friends.user_id_1 = ? AND friends.user_id_2 = users.id)) WHERE friends.accepted = 1");
$stmt->bind_param('ii', $_SESSION['id'], $_SESSION['id']);
$stmt->execute();
$stmt->bind_result($id, $name);
$stmt->store_result();
if ($stmt->num_rows > 0) {

    echo "<h2>Your Friends</h2>";
    echo "<ul>";

    while ($stmt->fetch()) {
        echo "<li>" . $name . " - <a href='?delete=$id'>Remove</a></li>";
    }

    echo "</ul>";
}
?>

<h2>Add Friends</h2>
<form action="" method="post">
    <label for="name">Friend's Username:</label>
    <input id="name" type="text" name="name">
    <input type="submit" name="submit" value="Send Request">
</form>
