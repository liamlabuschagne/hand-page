<?php
if (!isset($_SESSION['id'])) {
    header("Location: /login");
}

if (!empty($_POST['message'])) {
    $filters = [
        "message" => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
    ];
    $data = filter_var_array($_POST, $filters);

    $stmt = $dbc->prepare("INSERT INTO posts (user_id,message) VALUES (?,?)");
    $stmt->bind_param('is', $_SESSION['id'], $data['message']);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->affected_rows < 1) {
        echo "<p class='failure-alert'>Post failed.</p>";
    } else {
        echo "<p class='success-alert'>Post sent!</p>";
    }
}

?>
<h2>Post New Status</h2>
<form method="post">
    <br>
    <textarea name="message"></textarea>
    <br>
    <input type="submit" name="submit" value="Post">
</form>
<div id="timeline">
<?php
$stmt = $dbc->prepare("SELECT posts.user_id,posts.message,users.name FROM posts JOIN ( SELECT user_id_1 AS id FROM friends WHERE user_id_2 = ? UNION SELECT user_id_2 AS id FROM friends WHERE user_id_1 = ? UNION SELECT ? ) AS s JOIN users ON posts.user_id = users.id WHERE s.id = posts.user_id ORDER BY posts.id DESC");
$stmt->bind_param('iii', $_SESSION['id'], $_SESSION['id'], $_SESSION['id']);
$stmt->execute();
$stmt->bind_result($user_id, $message, $name);
while ($stmt->fetch()) {
    echo "<div class='post'>";
    if (file_exists("./img/profiles/" . $user_id)) {
        echo "<img class='profile-img' src='./img/profiles/" . $user_id . "' alt='" . $name . "\'s Profile Image'>";
    } else {
        echo "<img class='profile-img' src='./img/profiles/default' alt='Default Profile Image'>";
    }
    echo "<p><strong>" . $name . "</strong> " . $message . "</p>";
    echo "</div>";
}
?>
</div>