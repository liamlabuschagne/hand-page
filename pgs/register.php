<?php
if (!empty($_POST['name']) && !empty($_POST['password']) && !empty($_POST['password_repeat'])) {
    if (preg_match('/\s/', $_POST['name'])) {
        echo "<p class='failure-alert'>No spaces are allowed in your user name.</p>";
    } else {
        $filters = [
            "name" => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
            "password" => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
            "password_repeat" => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        ];
        $data = filter_var_array($_POST, $filters);

        if ($data['password'] != $data['password_repeat']) {
            echo "<p class='failure-alert'>Password and repeated password don't match.</p>";
        } else {

            $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

            // check if username is taken
            $stmt = $dbc->prepare("SELECT id FROM users WHERE name = ?");
            $stmt->bind_param('s', $data['name']);
            $stmt->execute();
            $stmt->store_result();
            if (!$stmt->num_rows < 1) {
                echo "<p class='failure-alert'>Username taken.</a>";
            } else {
                $stmt = $dbc->prepare("INSERT INTO users (name,password) VALUES (?,?)");
                $stmt->bind_param('ss', $data['name'], $data['password']);
                $stmt->execute();
                if ($stmt->affected_rows == 1) {
                    echo "<p class='success-alert'>Registered Successfully <a href='/login'>Login</a></p>";
                    exit();
                } else {
                    echo "<p class='failure-alert'>Registration Failed.</p>";
                }
            }
        }
    }
}
?>
<form method="post">
    <label for="name">Username:</label>
    <input id="name" type="text" name="name">
    <label for="password">Password:</label>
    <input id="password" type="password" name="password">
    <label for="password_repeat">Repeat:</label>
    <input id="password_repeat" type="password" name="password_repeat">
    <input type="submit" name="submit" value="Register">
</form>
