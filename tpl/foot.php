</main>
</div>
<footer>
    <p>HandPage - Made by Liam Labuschagne</p>
</footer>
<script>
    document.querySelector('#mobile-menu').addEventListener('click',()=>{
        document.querySelector('nav').style.display = "block";
    });

    document.querySelector('#mobile-close').addEventListener('click',()=>{
        document.querySelector('nav').style.display = "none";
    });
</script>
<script src="app.js"></script>
</body>
</html>
