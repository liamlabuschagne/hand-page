<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="128x91" href="favicon-home.png">
    <link rel="stylesheet" href="/css/base.css">
    <link rel="manifest" href="/manifest.json">
    <?php
if (file_exists('./css/' . $page . '.css')) {

    ?>
    <link rel="stylesheet" href="/css/<?php echo $page; ?>.css">
    <?php
}
?>
    <title><?php echo $pageName; ?></title>
</head>
<body>
    <div id="wrapper">
    <header>
        <div id="mobile-header">HandPage</div>
        <img id="mobile-menu" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABRSURBVGhD7dWxDQBBEAKx7b/p/wbQkRLY0oTEHAAw5huvSqOlqjRaqkqjpQDgKZ3HUlUaLVWl0VJVGi0FAE/pPJaq0mipKo2WqtJoKQBgxN0PZNbeMD5dwlgAAAAASUVORK5CYII=">
        <nav>
            <p id="mobile-close">X</p>
            <ul>
                <li>
                    <a href="/"><img src="./img/logo-large.png" alt="Logo"></a>
                </li>
            <?php
if (isset($_SESSION['id'])) {

    ?>
                <li><a href='/'>Home</a></li>
                <li><a href="/friends">Friends</a></li>
                <li><a href="/chat">Chat</a></li>
                <li><a href="/profile">Profile</a></li>
                <li><a href="/logout">Logout</a></li>
            <?php
} else {
    ?>
                <li><a href="/login">Login</a></li>
                <li><a href="/register">Register</a></li>
            <?php
}
?>
            </ul>
        </nav>
    </header>
    <main>
