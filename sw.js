self.importScripts('node_modules/sw-toolbox/sw-toolbox.js');
self.toolbox.precache(['/img/background-seemless.jpg', '/css/base.css', '/css/chat.css', '/css/friends.css', '/css/home.css', '/css/profile.css']);
self.toolbox.router.get('/*', toolbox.networkFirst);