<?php
session_start();
if (isset($_COOKIE['id']) && !empty($_COOKIE['id'])) {
    $_SESSION['id'] = $_COOKIE['id'];
}
include 'dbc.php';
if (isset($_GET['path'])) {
    $page = explode('/', $_GET['path'])[0];
} else {
    $page = "home";
}
$pageName = strtoupper($page[0]) . substr($page, 1);

include './tpl/head.php';
echo "<h1>" . $pageName . "</h1>";

$url_to_page = './pgs/' . $page . ".php";
if (file_exists($url_to_page)) {
    include $url_to_page;
}

include './tpl/foot.php';
